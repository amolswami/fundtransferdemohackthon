package com.fundtransfer.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FundTransferDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FundTransferDemoApplication.class, args);
	}

}
